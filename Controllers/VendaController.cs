using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api_davidbassouto.Context;
using tech_test_payment_api_davidbassouto.Entities;

namespace tech_test_payment_api_davidbassouto.Controllers
{
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost("add_prod")]
        public IActionResult Criar(Produto produto)
        {
            _context.Add(produto);
            _context.SaveChanges();
            return Ok(produto);

        }

        [HttpPost("add_vendedor")]
        public IActionResult Criar(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);

        }

        [HttpPost("nova_venda")]
        public IActionResult Criar(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }


        [HttpGet("obter_por_status")]
        public IActionResult ObterPorStatus(EnumVendaStatus status)
        {
            var venda = _context.Vendas.Where(x => x.status == status);

            return Ok(venda);
        }
    }
}
