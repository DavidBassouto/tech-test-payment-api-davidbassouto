using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api_davidbassouto.Entities
{
    public class Venda
    {
       
        public int Id { get; set; }
        public  DateTime data_venda { get; set; }
        public Vendedor vendedor { get; set; }
        public Produto produto { get; set; }
        public EnumVendaStatus status { get; set; }        
    }
}
